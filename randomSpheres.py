from world import *
from camera import *
from shape import *
from light import *
from ray import *
from material import *
import random

def randomSphere():
    x = random.random()*20 - 10
    y = random.random()*20 - 10
    z = random.random()*10 - 25 
    print([x,y,z])
    r = random.random()
    g = random.random()
    b = random.random()
    radius = random.random()*3 + 1
    return Sphere((x,y,z),radius,Phong(color=(r,g,b)))

class RandomSpheres(World):
    def __init__(self,n=20,lights=None,camera=None):
        objects = [randomSphere() for i in range(n)]
        self.camera = Camera()
        World.__init__(self,objects,[Light()],self.camera)
    
    def colorAt(self,x,y):
        vctr = Camera.ray(Camera(),x,y)
        #print("point = " + str(vctr.point) + "  x = " + str(x)+ "  y = " + str(y))
       # print("x = " + str(x) + " y = " + str(y))
        hit = Ray.closestHit(vctr,self)
        if not hit  == None:
            #print("HIT")
            return hit[3].material.colorAt(hit[1], hit[2],vctr,self)
        return self.neutral

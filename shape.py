import numpy
import vector
import ray
from material import *

EPSILON = 1.0e-10

class GeometricObject():
    def hit(self, ray):
        """Returns (t, point, normal, object) if hit
            and t > EPSILON"""
        """t is distance from self.point"""              
        
        return (None, None, None, None)
    def castShadows(self):
        return False

class Plane(GeometricObject):
    def __init__(self,point=(0,-10,-20), normal=(0,-1,0),material=(Flat())):
        self.point = point
        self.normal = vector.normalize(normal)
        self.material = material
        
    def hit(self,ray):
        t = 0 
        if(numpy.dot(self.normal,ray.vector) != 0):
            t = numpy.dot(self.normal,(self.point-ray.point))/numpy.dot(self.normal,ray.vector) 
        if(t >= EPSILON):
            intersect = ray.point + t*ray.vector
            return(t,intersect,self.normal,self)
        return None
    
    def castShadows(self):
        return False

class Sphere(GeometricObject):
    def __init__(self, point=(0,0,0), radius=1,
                 material=None):
        if not(material):
            material = Phong()
        self.point = point
        self.radius = radius
        self.material = material
        
    def castShadows(self):
        return True

#    def hit(self,ray):
#        #print(ray.point)
#        #print("Vector = " + str(ray.vector))
#        point = ray.point - self.point
#        #print("Point = " + str(point))
#        a = numpy.dot(ray.vector,ray.vector)
##        print("a = " + str(a))
#        b = 2 * numpy.dot(ray.vector, point)
##        print("b = " + str(b))
#        c = numpy.dot(point,point) - self.radius**2
##        print("c = " + str(c))
#        
#        disc = b**2 - 4*a*c
#        #print("Disc = " + str(disc))
#        if disc > 0.0:
#            discRoot = numpy.sqrt(disc)
#            t0 = (-b - discRoot) / 2*a 
#            t1 = (-b + discRoot) / 2*a
##            print(t0)
##            print(t1)
##            print
#            
#            
#
#            t0,t1 = min(t0,t1), max(t0,t1)
#            if t1 >= EPSILON:
#              
#
#                if t0 < EPSILON:
#                    intersect = ray.point + t1 * ray.vector
#                    return (t1, intersect, vector.normalize(intersect - self.point), self)
##                    
#                else :
#                   intersect = ray.point + t0 * ray.vector
#                   return (t0, intersect, 
#                            vector.normalize(intersect - self.point), self)
    def hit(self, ray):
        # assume sphere at origin, so translate ray:
        raypoint = ray.point - self.point
        a = numpy.dot(ray.vector, ray.vector)
        b = 2*numpy.dot(raypoint, ray.vector)
        c = numpy.dot(raypoint, raypoint) - self.radius*self.radius
        disc = b*b - 4*a*c
        if disc > 0.0:
            t = (-b-numpy.sqrt(disc))/(2*a)
            if t > EPSILON:
                p = ray.pointAt(t)
                n = normalize(p - self.point)
                return (t, p, n, self)
            t = (-b+numpy.sqrt(disc))/(2*a)
            if t > EPSILON:
                p = ray.pointAt(t)
                n = normalize(p - self.point)
                return (t, p, n, self)        
        return None
                            
                            
    
import numpy
import sympy

def vec(x,y=None,z=None):
    if(y != None and z != None):
        return numpy.array([x,y,z], dtype=numpy.float32)
    return numpy.array(x, dtype=numpy.float32)
    """return a numpy.array with type numpy.float"""

def normalize(v):
    """return a normalized vector in direction v"""
    temp =  v / numpy.linalg.norm(v)
    return temp
    
def reflect(v, normal):
    """return v reflected through normal"""
    if numpy.dot(v,normal) < 0:
        normal = -normal
#        print("negative NORMAL")
#    return v-2*(v-numpy.dot(v,normal)*normal)#
    return 2*(numpy.dot(v,normal)*normal) - v
    